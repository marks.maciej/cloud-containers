#!/usr/bin/env python


from flask import Flask, request

app = Flask(__name__)


@app.route('/')
def root():
    return 'Witaj w aplikacji pomagającej testować skalowalność aplikacji za pomocą Kubernetes!\n'

@app.route('/login',  methods=['GET', 'POST'])
def login():
    deviceid = request.values.get('deviceid')
    return '/login - device: {}\n'.format(deviceid)

@app.route('/<name>')
def hello_name(name):
    return "Hello {}!".format(name)    

@app.route('/metrics',  methods=['GET', 'POST'])
def metrics():
    deviceid = request.values.get('deviceid')
    timestamp = request.values.get('timestamp')
    
    return '/metrics - device: {}, timestamp: {}\n'.format(deviceid, timestamp)



if __name__ == '__main__':
    # Używane tylko lokalnie
    app.run(host='127.0.0.1', port=8080, debug=True)
